#ifndef QUANTUMTAR_H_
#define QUANTUMTAR_H_

#define USTAR_SIZE 5
#define SIZE_OCTAL_BASE 11
#define TAR_BLOCK_SIZE 512

#define size_t unsigned int

struct tar_header {
    char filename[100];   // filename
    char mode[8];         // file mode
    char uid[8];          // user id
    char gid[8];          // group id
    char size[12];        // size  (octal)
    char mtime[12];       // modification time (octal)
    char chksum[8];       // checksum
    char typeflag[1];     // type flag
    char linkname[100];   // linked filename
    char ustar[5];        // ustar\0
    char ustarver[3];     // 00
    char ownername[32];   // owner file username
    char ownergroup[32];  // owner group name
    char devmajornum[8];  // device major number
    char devminornum[8];  // device minor number
    char fileprefix[155]; // filename prefix
    char align[12];       // align sizeof(struct tar_header) to 512 bytes
};

enum tar_typeflag {
    TAR_NORMALFILE = 0,     // normal file
    TAR_HARDLINK,           // hard link
    TAR_SYMBLINK,           // symbolic link
    TAR_CHARDEV,            // character device
    TAR_BLOCKDEV,           // block device
    TAR_DIRECTORY,          // directory
    TAR_NAMEDPIPE,          // named pipe (FIFO)
};

/* our memcmp implementation, so we don't need libc :-) */
unsigned char
tar_memcmp(const void *s1, const void *s2, size_t n) {
    const unsigned char *p1 = (const unsigned char *) s1;
    const unsigned char *p2 = (const unsigned char *) s2;

    for (size_t i = 0; i < n; i++) {
        if (p1[i] != p2[i]) {
            return (p1[i] < p2[i]) ? -1 : 1;
        }
    }

    return 0;
}

/* our strlen implementation */
unsigned int 
tar_strlen(const char *s) {
    unsigned int count = 0;

    while(*s != '\0') {
        count++;
        s++;
    }

    return count;
}

/* 
   Function for converting octal number to binary integer
   size - size of string in pointer
   *str - pointer to string containing octal number 
*/
int 
tar_oct2bin(unsigned char *str, int size) {
    int n = 0;
    unsigned char *c = str;
    while (size-- > 0) {
        n *= 8;
        n += *c - '0';
        c++;
    }
    return n;
}


/* returns file size and pointer to file data in out */
int tar_lookup(unsigned char *archive, char *filename, char **out) {
    unsigned char *archive_ptr = archive;

    struct tar_header* archive_data = (struct tar_header*)archive_ptr;
    
    /* Loop through the archive until the end or the file is found */
    while (!tar_memcmp(&archive_data->ustar, "ustar", USTAR_SIZE)) {
        int filesize = tar_oct2bin((unsigned char*)&archive_data->size, SIZE_OCTAL_BASE);
	
	/* Check if the current file in the archive matches the filename */
	while (!tar_memcmp(&archive_data->filename, filename, tar_strlen(filename) + 1)) {
	   *out = (char*)archive_ptr + sizeof(struct tar_header);
	   return filesize;
	}
	
	/* Move the pointer to the next file in the archive */
	archive_ptr += (((filesize + TAR_BLOCK_SIZE - 1) / TAR_BLOCK_SIZE) + 1) * TAR_BLOCK_SIZE;
    }
    
    /* Return -1 if the file was not found in the archive */
    return -1;
}

int tar_ls(unsigned char* archive, char** files) {
    unsigned char *archive_ptr = archive;
    int file_index = 0;

    while (1) {
        struct tar_header* archive_data = (struct tar_header*)archive_ptr;

        /* Check if we've reached the end of the archive */
        if (tar_memcmp(&archive_data->ustar, "ustar", USTAR_SIZE) != 0) {
            break;
        }

        /* Add the filename to the files array */
        files[file_index] = archive_data->filename;
        file_index++;

        /* Calculate the size of the file */
        int filesize = tar_oct2bin((unsigned char*)&archive_data->size, SIZE_OCTAL_BASE);

        /* Move the pointer to the next file in the archive */
        archive_ptr += (((filesize + TAR_BLOCK_SIZE - 1) / TAR_BLOCK_SIZE) + 1) * TAR_BLOCK_SIZE;
    }

    /* Null-terminate the files array */
    files[file_index] = NULL;

    return file_index;
}

int tar_count_files(unsigned char* archive) {
    unsigned char *archive_ptr = archive;
    int file_count = 0;

    while (1) {
        struct tar_header* archive_data = (struct tar_header*)archive_ptr;

        /* Check if we've reached the end of the archive */
        if (tar_memcmp(&archive_data->ustar, "ustar", USTAR_SIZE) != 0) {
            break;
        }

        /* Increment the file count */
        file_count++;

        /* Calculate the size of the file */
        int filesize = tar_oct2bin((unsigned char*)&archive_data->size, SIZE_OCTAL_BASE);

        /* Move the pointer to the next file in the archive */
        archive_ptr += (((filesize + TAR_BLOCK_SIZE - 1) / TAR_BLOCK_SIZE) + 1) * TAR_BLOCK_SIZE;
    }

    return file_count;
}

int tar_exists(unsigned char* archive, char* filename) {
    unsigned char *archive_ptr = archive;

    while (1) {
        struct tar_header* archive_data = (struct tar_header*)archive_ptr;

        /* Check if we've reached the end of the archive */
        if (tar_memcmp(&archive_data->ustar, "ustar", USTAR_SIZE) != 0) {
            break;
        }

        /* Check if the filename matches */
        if (tar_memcmp(&archive_data->filename, filename, tar_strlen(filename) + 1) == 0) {
            return 0;
        }

        /* Calculate the size of the file */
        int filesize = tar_oct2bin((unsigned char*)&archive_data->size, SIZE_OCTAL_BASE);

        /* Move the pointer to the next file in the archive */
        archive_ptr += (((filesize + TAR_BLOCK_SIZE - 1) / TAR_BLOCK_SIZE) + 1) * TAR_BLOCK_SIZE;
    }

    /* If we've reached this point, the file was not found in the archive */
    return -1;
}

enum tar_typeflag tar_typeof(unsigned char* archive, char* filename) {
    unsigned char *archive_ptr = archive;

    while (1) {
        struct tar_header* archive_data = (struct tar_header*)archive_ptr;

        /* Check if we've reached the end of the archive */
        if (tar_memcmp(&archive_data->ustar, "ustar", USTAR_SIZE) != 0) {
            return -1;
        }

        /* Check if the filename matches */
        if (tar_memcmp(&archive_data->filename, filename, tar_strlen(filename) + 1) == 0) {
            /* Return the type of the file */
            switch (archive_data->typeflag[0]) {
                case '0': return TAR_NORMALFILE;
                case '1': return TAR_HARDLINK;
                case '2': return TAR_SYMBLINK;
                case '3': return TAR_CHARDEV;
                case '4': return TAR_BLOCKDEV;
                case '5': return TAR_DIRECTORY;
                case '6': return TAR_NAMEDPIPE;
                default: return -1; /* Unknown type */
            }
        }

        /* Calculate the size of the file */
        int filesize = tar_oct2bin((unsigned char*)&archive_data->size, SIZE_OCTAL_BASE);

        /* Move the pointer to the next file in the archive */
        archive_ptr += (((filesize + TAR_BLOCK_SIZE - 1) / TAR_BLOCK_SIZE) + 1) * TAR_BLOCK_SIZE;
    }

    /* If we've reached this point, the file was not found in the archive */
    return -1;
}

#endif /* QUANTUMTAR_H_ */

