# TODO

### `tar_ls` function

The `tar_ls` function will need to look something like this:
```c
int tar_ls(unsigned char* archive, char** files);
```

Where `archive` is a pointer to the archive data; `files` is a pointer to an array of char*'s that will contain file names.


### `tar_ls` function was created at 2023-08-29 10:04 PM
