# quantumtar.h docs

## Functions

### `tar_memcmp` and `tar_strlen`

`tar_memcmp` and `tar_strlen` was designed for zero dependencies from libc in `quantumtar.h` :-)

### `tar_oct2bin`

`tar_oct2bin` converts octal number in `*str` to binary number. Used by `tar_lookup` function.

### `tar_lookup`

`tar_lookup` functions has three arguments:

```c
unsigned char *archive,
char* filename,
char** out
```

`archive` is pointer to archive.

`filename` - filename that we are searching in archive

`out` - pointer to string were we will store pointer to data of file.

Returns size of file if file was founded. Else returns -1.

### `tar_ls`

`tar_ls` has two arguments:

```c
unsigned char* archive, 
char** files
```

`archive` is pointer to archive :-))

`files` pointer to `char*` array. This array will contain files that was founded in archive.

Returns count of files in archive. Else returns -1.

### `tar_count_files`

`tar_count_files` has only one argument:

```c
unsigned char* archive
```

`archive` is pointer to archive :-))))

Returns count of files in archive, else returns -1.

### `tar_exists`

`tar_exists` has two arguments:

```c
unsigned char* archive,
char* filename
```

`archive` is pointer to archive..

`filename` is name of file that we are checking to exists in archive.

Return 0 if file exists, else returns -1.

### `tar_typeof`

`tar_typeof` has two arguments:

```c
unsigned char* archive,
char* filename
```

`archive` is pointer to archive

`filename` is name of file that type we want to find.

Returns `enum tar_typeflag`. Else returns -1.


### About `enum tar_typeflag`

Here are code of it:

```c
enum tar_typeflag {
    TAR_NORMALFILE = 0,     // normal file
    TAR_HARDLINK,           // hard link
    TAR_SYMBLINK,           // symbolic link
    TAR_CHARDEV,            // character device
    TAR_BLOCKDEV,           // block device
    TAR_DIRECTORY,          // directory
    TAR_NAMEDPIPE,          // named pipe (FIFO)
};
```

`enum tar_typeflag` is created to simplify code writing and contains tar archive file types.

